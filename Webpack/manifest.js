import { join } from 'path';
const NODE_ENV = process.env.NODE_ENV || 'development';
const IS_DEVELOPMENT = NODE_ENV === 'development';
const IS_PRODUCTION = NODE_ENV === 'production';

const paths = {
    root: join(__dirname, '../'),
    src: join(__dirname, '../WebAssets'),
    build: join(__dirname, '../wwwroot/dev'),
    dist: join(__dirname, '../wwwroot/dist')
};

export default {
    NODE_ENV,
    IS_DEVELOPMENT,
    IS_PRODUCTION,
    paths
};