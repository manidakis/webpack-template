import { join } from 'path';
import manifest from './manifest';
import rules from './rules';
import plugins from './plugins';
import optimization from './optimization';

const entry = [
    join(manifest.paths.src, 'scripts', 'index.js')
];

const resolve = {
    extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js'],
    modules: [
        join(__dirname, '../node_modules'),
        join(manifest.paths.src, ''),
    ]
};

let output = {};

if (manifest.IS_DEVELOPMENT) {
    output = {
        path: manifest.paths.build,
        filename: '[name].js'
    };
}

if (manifest.IS_PRODUCTION) {
    output = {
        path: manifest.paths.dist,
        filename: '[name].js'
    };
}

export default {
    mode: manifest.NODE_ENV,
    watch: !manifest.IS_PRODUCTION,
    entry,
    output: output,
    module: {
        rules,
    },
    optimization,
    resolve,
    plugins
}