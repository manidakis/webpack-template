import splitChunks from './splitChunks';
import minimizer from './minimizer';

export default {
    splitChunks,
    minimizer
};
