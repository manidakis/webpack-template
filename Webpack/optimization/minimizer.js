import manifest from '../manifest';
import UglifyJSPlugin from 'uglifyjs-webpack-plugin';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';

export default [
    new UglifyJSPlugin({
        cache: true,
        parallel: true,
        sourceMap: manifest.IS_DEVELOPMENT
    }),
    new OptimizeCssAssetsPlugin()
];