export const test = /\.(png|gif|jpg|jpeg|svg)$/i;
export const exclude = /(node_modules)/;
export const use = [
    {
        loader: 'file-loader',
        options: {
            outputPath: 'images'
        }
    }
];