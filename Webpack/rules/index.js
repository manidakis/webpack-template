export default [
    require('./js'),
    require('./images'),
    require('./css'),
    require('./sass'),
    require('./fonts'),
];