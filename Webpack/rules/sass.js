import { join } from 'path';
import manifest from '../manifest';
import { loader as _loader } from 'mini-css-extract-plugin';

let loaders = [];

loaders = [
    {
        loader: 'css-loader',
        options: {
            sourceMap: manifest.IS_DEVELOPMENT,
            minimize: manifest.IS_PRODUCTION
        }
    },
    {
        loader: 'sass-loader',
        options: {
            sourceMap: manifest.IS_DEVELOPMENT,
            includePaths: [
                join(manifest.paths.root, 'node_modules'),
                join(manifest.paths.src, 'styles'),
                join(manifest.paths.src, '')
            ]
        }
    }
];

if (manifest.IS_DEVELOPMENT) {
    loaders = [
        {
            loader: 'style-loader'
        }
    ].concat(loaders);
}

if (manifest.IS_PRODUCTION) {
    loaders = [
        {
            loader: _loader
        }
    ].concat(loaders);
}

export const test = /\.scss$/;
export const use = loaders;