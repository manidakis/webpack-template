import manifest from '../manifest';
import { loader as _loader } from 'mini-css-extract-plugin';

let loaders = [];

loaders = [
    {
        loader: 'css-loader',
        options: {
            sourceMap: manifest.IS_DEVELOPMENT,
            minimize: manifest.IS_PRODUCTION
        }
    }
];

if (manifest.IS_DEVELOPMENT) {
    loaders = [
        {
            loader: 'style.css'
        }
    ].concat(loaders);
}

if (manifest.IS_DEVELOPMENT) {
    loaders = [
        {
            loader: _loader
        }
    ].concat(loaders);
}

export const test = /\.css$/;
export const use = loaders;