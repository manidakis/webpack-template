export const test = /\.(js)$/;
export const exclude = /(node_modules|build|dist\/)/;
export const use = ['babel-loader'];