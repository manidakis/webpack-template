export const test = /\.(eot|svg|ttf|woff|woff2)$/;
export const exclude = /(node_modules)/;
export const use = [
    {
        loader: 'file-loader',
        options: {
            outputPath: 'fonts'
        }
    }
];