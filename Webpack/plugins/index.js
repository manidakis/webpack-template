import manifest from '../manifest';

const plugins = [];

plugins.push(
    ...(require('./internal').default),
    require('./caseSensitivePlugin').default,
);

if (manifest.IS_PRODUCTION) {
    plugins.push(
        require('./extractPlugin').default,
        require('./copyPlugin').default
    );
}

export default plugins;