import { join } from 'path';
import manifest from '../manifest';
import CopyWebpackPlugin from 'copy-webpack-plugin';

export default new CopyWebpackPlugin([
    {
        from: join(manifest.paths.src, 'static'),
        to: manifest.IS_DEVELOPMENT ? manifest.paths.build : manifest.paths.dist,
    },
]);
