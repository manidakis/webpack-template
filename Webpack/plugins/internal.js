import webpack from 'webpack';
import manifest from '../manifest';

const plugins = [];

plugins.push(
    new webpack.DefinePlugin(
        {
            'process.env': {
                NODE_ENV: JSON.stringify(manifest.NODE_ENV)
            }
        }
    )
);

plugins.push(
    new webpack.ProvidePlugin(
        {
            $: 'jquery',
            jQuery: 'jquery',
            'window.jquery': 'jquery',
            Popper: ['popper.js', 'default']
        }
    )
);

if (manifest.IS_DEVELOPMENT) {
    plugins.push(
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    );
}

export default plugins;